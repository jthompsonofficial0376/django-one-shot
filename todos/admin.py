from django.contrib import admin
from todos.models import TodoList, TodoItem
# Register your models here.

@admin.register(TodoList) 
class TodoListAdmin(admin.ModelAdmin):
    list_display = [ 
        "id",
        "name", 
        "created_on"
    ]

@admin.register(TodoItem)
class TodoItem(admin.ModelAdmin):
    list_display = [ 
        "task",
        "due_date",
        "is_completed",
    ]
    





'''
# The old Admin.py - 
from django.contrib import admin
from recipes.models import Recipe

# Register your models here.
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list = [ 
        "title", 
        "id",
    ]
'''