from django.shortcuts import render , get_object_or_404 , redirect
from .models import TodoList
from .forms import TodoListForm


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    print(todo_lists)  # Retrieve all TodoList instances
    context = {'todo_lists': todo_lists} 
    
    
     # Add them to the context
    return render(request, 'todos/todo.html', context)  # Render the template

# All of the objects 

def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = { "todo_list_detail": todo_list_detail } 
    return render(request, 'todos/todo_list_detail.html', context )

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_detail", id=new_list.id )

    else:
        form = TodoListForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context )



def todo_list_edit(request, id):
    post = get_object_or_404( id=id)

    if request.method == "POST": 
        form = TodolistForm(request.POST, instance=post)
        if form.isvalid():
            form.save()
            return redirect("todo_list_edit", id=id)
