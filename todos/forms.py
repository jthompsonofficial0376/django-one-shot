from django.forms import ModelForm
from todos.models import TodoList

class TodoListForm(ModelForm):
    class Meta:    # Meta is a special subclass in django
        model = TodoList  # - model -special - key reference word - 
        fields = [          # - feature-special - key reference word - 
            "name",
        ]


#  from django.forms import ModelForm
# from recipes.models import Recipe


# class RecipeForm(ModelForm):    # Step 1
#     class Meta:                 # Step 2
#         model = Recipe          # Step 3
#         fields = [              # Step 4
#             "title",            # Step 4
#             "picture",          # Step 4
#             "description",      # Step 4
#         ]                       # Step 4