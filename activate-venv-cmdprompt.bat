@echo off

:: Navigate to the root directory of the project
cd /d "%~dp0\.."

:: Activate the virtual environment using PowerShell
powershell -NoExit -Command "& '%~dp0\.venv\Scripts\Activate.ps1';" 

:: Navigate to the root directory of the project
cd /d "%~dp0\.."


python manage.py runserver"
