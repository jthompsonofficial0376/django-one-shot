:: Navigate to the root directory of the project
cd /d "%~dp0\.."

:: Open a new PowerShell session in the root directory
start powershell -NoExit -Command "& '%~dp0\.venv\Scripts\Activate.ps1'; 

